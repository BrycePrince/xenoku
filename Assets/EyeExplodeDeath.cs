﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EyeExplodeDeath : MonoBehaviour {

    private AudioSource audSource;
    
    [SerializeField] private AudioClip[] audioClips;

	// Use this for initialization
	void Start () {
        audSource = GetComponent<AudioSource>();
        audSource.clip = audioClips[Random.Range(0, 1)];
       // audSource.Play();
        audSource.PlayOneShot(audioClips[Random.Range(0, audioClips.Length)], .3f);
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
