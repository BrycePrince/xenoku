﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pigDeathAudio : MonoBehaviour {

    private AudioSource audioSource;
    [SerializeField] private AudioClip[] audioClips;

	// Use this for initialization
	void Start () {
        audioSource = GetComponent<AudioSource>();
        audioSource.PlayOneShot(audioClips[Random.Range(0, audioClips.Length)]);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
