﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class laserRender : MonoBehaviour {

    [Header("LineRenderer Attributes")]
    [SerializeField] private LineRenderer enemyLineLaser;
    [SerializeField] private GameObject pointA;
    [SerializeField] private GameObject pointB;
    [SerializeField] private GameObject shootPoint;
    private GameObject player;

    public PlayerControllerScript playerControllerScript;

    // Use this for initialization
    void Start () {
        //get player object and scripot
        player = GameObject.Find("Player");
        playerControllerScript = player.GetComponentInParent<PlayerControllerScript>();

        Rigidbody rigidbody = GetComponent<Rigidbody>();
        //find enemy shootpoint
        shootPoint = GameObject.Find("ShootPoint");
        transform.rotation = Quaternion.LookRotation(shootPoint.transform.position - transform.position);
        enemyLineLaser.enabled = true;
    }
	
	// Update is called once per frame
	void Update ()
    {
        GetComponent<Rigidbody>().AddForce(transform.forward * 8);
        //transform.Translate(transform.position * Time.deltaTime * 1);
        enemyLineLaser.SetPosition(0, pointA.transform.position);
        enemyLineLaser.SetPosition(1, pointB.transform.position);
    }

    private void Death()
    {
        Destroy(this.gameObject);
    }

    void OnTriggerEnter(Collider coll)
    {
        //is this object a grenade
        if (coll.gameObject.tag == "Player")
        {
            playerControllerScript.DealDamage(1);
            Death();
        }
        //if this object is not a grenade
        else
        {
            
        }
    }
}
