﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Author/s:Bryce Prince
//Date:29/05/18
//Function:holds the behaviour of pickups
public class PickUpsScript : MonoBehaviour 
{
	[Tooltip("type = Sheilds/Health")]
	public string type;
	public int amount;

	private GameObject player;
	private PlayerControllerScript playerControllerScript;

	// Use this for initialization
	void Start()
	{
		//find the player
		player = GameObject.Find ("Player");
		//find the playerControllerScript
		playerControllerScript = player.GetComponentInParent<PlayerControllerScript> ();
	}

	//did we hit something
	void OnTriggerEnter(Collider coll)
	{
		//was it the player
		if(coll.gameObject.tag == "Player")
		{
			//call the healing function on the object we hit (player)
			playerControllerScript.DealHealing (type, amount);
			//remove this object from the scene
			Destroy (this.gameObject);
		}
	}
}