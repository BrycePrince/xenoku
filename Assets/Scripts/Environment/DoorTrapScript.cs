﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Author/s:Bryce Prince
//Date:24/05/18
//Function:Holds behaviour for the door traps
public class DoorTrapScript : MonoBehaviour 
{
	//make this class visible in the editor
	[System.Serializable]
	//Function:holds Variables for the Door Class
	public class Doors
	{
		//thisis the part we will be moving
		public GameObject doorRef;
		//the start point of our doors
		public GameObject upPoint;
		//the "Down" point of our doors
		public GameObject downPoint;
	}

	//determines when the trap should revert
	public bool release;
	//determines if the trap hs triggered
	public bool triggered;

	public GameObject fightAreaCentrePoint;
	public float fightAreaRadius;

	//array of doors
	public Doors[] doors;

	//Use this for initialization
	void Start()
	{
		//for every door that is in our array
		foreach (Doors door in doors) 
		{
			//move the door to the up position
			door.doorRef.transform.position = door.upPoint.transform.position;
		}
	}

	//Update is called once per frame
	void Update()
	{
		//check for the presence of each enemy type
		GameObject[] eyebots = GameObject.FindGameObjectsWithTag("EnemyEyebot");
		GameObject[] pigs = GameObject.FindGameObjectsWithTag("EnemyPig");
		GameObject[] enemyThrees = GameObject.FindGameObjectsWithTag("EnemySassySama");

		//check if the trap has been triggered
		if(triggered == true)
		{
			//how many of each enemy
			if(eyebots.Length == 0 &&
				pigs.Length == 0 &&
				enemyThrees.Length == 0)
			{
				//if none then release the trap
				release = true;
			}
		}

		//if the trap sequence is complete
		if(release == true)
		{
			//release the trap
			Release ();
		}
	}

	//moves the doors up
	void Release()
	{
		//for every door that is in our array
		foreach (Doors door in doors) 
		{
			//move the door to the up position
			door.doorRef.transform.position = door.upPoint.transform.position;
		}
	}

	//did something hit us
	void OnTriggerEnter(Collider coll)
	{
		//was it the player
		if(coll.gameObject.tag == "Player")
		{
			//stops the trap from releasing before it has triggered
			triggered = true;
			//for every door that is in our array
			foreach (Doors door in doors) 
			{
				//moves the doors up
				door.doorRef.transform.position = door.downPoint.transform.position;
			}
		}
	}
}