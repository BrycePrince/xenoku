﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Author(s)/Modifier(s):Bryce Prince
//Date:16/07/18
//Function:trigger controller for wallfading
public class WallCamera : MonoBehaviour 
{
	[SerializeField] private float resetTime;
	private Transform player;
	private Transform cameraTransform;
	private Vector3 velocity;

	// Use this for initialization
	void Start()
	{
		//find the player transform
		player = GameObject.Find("Player Object").transform;
		//find the camera transform
		cameraTransform = Camera.main.gameObject.transform;
	}

	// Update is called once per frame
	void Update()
	{
		//find the difference between the player and camera transforms
		Vector3 difference = cameraTransform.position - player.transform.position;
		//position between camera and player
		Vector3 position = cameraTransform.position - (difference/2);
		//move this object to that position
		transform.position = Vector3.SmoothDamp(transform.position, position, ref velocity, 0.0f);
		//point this object at the player
		transform.LookAt(player.transform.position + new Vector3 (0,1,0));
	}

	//did we hit a trigger
	void OnTriggerEnter(Collider other)
	{
		//was it an object with the environment tag?
		if (other.gameObject.tag == "Environment")
		{
			//change the material to the transparent version
			other.gameObject.GetComponentInParent<WallHider>().StartFadeTransparent();
		}
	}

	//did we stop hitting a trigger
	void OnTriggerExit(Collider other)
	{
		//was it an object with the environment tag?
		if (other.gameObject.tag == "Environment")
		{
			//change the material back to the opaque version
			other.gameObject.GetComponentInParent<WallHider>().Invoke("StartFadeOpaque", resetTime);
		}
	}
}