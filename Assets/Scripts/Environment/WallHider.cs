﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallHider : MonoBehaviour 
{
	private MeshRenderer meshRenderer;

	[SerializeField] private float transparentValue = 0.25f;
	[SerializeField] private float opaqueValue = 1.0f;
	private float fadeRate;
	// Use this for initialization
	void Start () 
	{
		fadeRate = opaqueValue / 100;

		//find the meshRenderer
		meshRenderer = GetComponent<MeshRenderer> ();
	}

	// Update is called once per frame
	void Update () 
	{
		
	}

	public void StartFadeTransparent()
	{
		//stop fading to opaque
		StopCoroutine("SwitchToOpaque");
		//start fading to transparent
		StartCoroutine ("FadeToTransparent");
	}

	//fade from 1 to 0.25
	public IEnumerator FadeToTransparent()
	{
		// loop over 1 second backwards
		for (float i = meshRenderer.material.GetFloat ("_Transparency"); i > transparentValue; i -= fadeRate)
		{
			//set transparency with i as alpha
			meshRenderer.material.SetFloat ("_Transparency", i);

			if(meshRenderer.material.GetFloat("_Transparency") <= (transparentValue + 0.01f))
			{
				//stop this function
				StopCoroutine("FadeToOpaque");
			}

			//return nothing
			yield return null;
		}
	}

	public void StartFadeOpaque()
	{
		//stop fading to transparent
		StopCoroutine("FadeToTransparent");
		//start fading to opaque
		StartCoroutine ("FadeToOpaque");
	}

	public IEnumerator FadeToOpaque()
	{
		// loop over 1 second
		for (float i = meshRenderer.material.GetFloat ("_Transparency"); i < opaqueValue; i += fadeRate)
		{
			//set transparency with i as alpha
			meshRenderer.material.SetFloat ("_Transparency", i);

			if(meshRenderer.material.GetFloat("_Transparency") >= (opaqueValue - 0.01f))
			{
				//stop this function
				StopCoroutine("FadeToOpaque");
			}


			//return nothing
			yield return null;
		}
	}
}