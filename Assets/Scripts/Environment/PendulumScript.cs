﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Author/s:Bryce Prince
//Date:24/05/18
//Function:holds behaviour for the pendulum trap
public class PendulumScript : MonoBehaviour 
{
	[SerializeField] private int damage;
	[SerializeField] private float knockbackForce;
	[SerializeField] private float stunTime;
	[SerializeField] private float maxSwingAngle;
	[SerializeField] private float swingSpeed;
	private GameObject swingPoint;
	private GameObject player;
	private Rigidbody playerRigid;
	private PlayerControllerScript playerControllerScript;
	private bool goingForwards;

	//Use this for initialization
	void Start () 
	{
		//find the player
		player = GameObject.Find ("Player");
		//find the players rigidbody component
		playerRigid = player.GetComponentInParent<Rigidbody> ();
		//find the playerControllerScript on the player;
		playerControllerScript = player.GetComponentInParent<PlayerControllerScript> ();
		//find the point that we will rotate around
		swingPoint = transform.parent.gameObject;
	}

	//Update is called once per frame
	void Update()
	{
		//if we are rotating forwards
		if(goingForwards == true)
		{
			//have we reached the maximum value
			if(swingPoint.transform.rotation == Quaternion.Euler (-maxSwingAngle, 0, 0))
			{
				//start rotating the opposite way
				goingForwards = false;

			}
			//if we haven't reached the maximum value
			else if(swingPoint.transform.rotation != Quaternion.Euler (-maxSwingAngle, 0, 0))
			{
				//rotate the object
				swingPoint.transform.rotation = Quaternion.RotateTowards (swingPoint.transform.rotation, Quaternion.Euler (-maxSwingAngle, 0, 0), swingSpeed * Time.deltaTime);
			}
		}
		//if we are rotating backwards
		else if(goingForwards == false)
		{
			//have we reached the maximum value
			if(swingPoint.transform.rotation == Quaternion.Euler (maxSwingAngle, 0, 0))
			{
				//start rotating the opposite way
				goingForwards = true;
			}
			//if we haven't reached the maximum value
			else if(swingPoint.transform.rotation != Quaternion.Euler (maxSwingAngle, 0, 0))
			{
				//rotate the object
				swingPoint.transform.rotation = Quaternion.RotateTowards (swingPoint.transform.rotation, Quaternion.Euler (maxSwingAngle, 0, 0), swingSpeed * Time.deltaTime);
			}
		}
	}

	//releases the player
	void ReleasePlayer()
	{
		//release the player
		playerControllerScript.playerStilled = false;
	}

	//did we hit something
	void OnCollisionEnter(Collision coll)
	{
		//was it the player
		if(coll.gameObject.tag == "Player")
		{
			//remove velocity from the player
			playerRigid.velocity = Vector3.zero;
			//remove angular velocity from the player (spin)
			playerRigid.angularVelocity = Vector3.zero;
			//stop the player from giving input
			playerControllerScript.playerStilled = true;

			//find the directing to move the player to
			Vector3 heading = player.transform.position - transform.position;

			//allow the player to regain control after the stun time
			Invoke ("ReleasePlayer", stunTime);

			//knock the player away from the trap
			playerRigid.AddForce (heading * knockbackForce, ForceMode.Impulse);
			//deal damage to the player
			playerControllerScript.DealDamage (damage);
		}
	}
}