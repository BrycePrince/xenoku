﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

//Author/s:Bryce Prince
//Date:23/06/18
//Function:holds behaviours for the Eyebot enemy
public class EnemyEyebot : EnemyAI 
{
	[Header("Shoot Settings")]
	[SerializeField] private float shootRange;
	[SerializeField] private float shootCooldownTime;
	private float shootCooldown;
	public int damageAttackOne;

	[Header("Beam Settings")]
	[SerializeField] private float beamRange;
	[SerializeField] private float beamCooldownTime;
	private float beamCooldown;
	public int damageAttackTwo;
	[SerializeField] private float beamWindUpTime;
	[SerializeField] private float beamDuration;
	[SerializeField] private GameObject beamHitbox;
	private Vector3 target;
    [SerializeField] private LineRenderer eyeLaser;
    [SerializeField] private GameObject startPoint;
    [SerializeField] private GameObject endPoint;
    [SerializeField] private ParticleSystem BallStart;
    [SerializeField] private ParticleSystem BallEnd;
    [SerializeField] private ParticleSystem Flash;
    [SerializeField] private ParticleSystem DeathParticle;
    [SerializeField] private ParticleSystem BaseDeathParticle;
    [SerializeField] private GameObject explodeOb;
    [SerializeField] private GameObject deathOb;
    [SerializeField] private GameObject littleLaser;

    [Header("Explode Settings")]
	[SerializeField] private float explodeTime;
	[SerializeField] private float explodeRange;
	[Tooltip("explode")] public int damageAttackThree;
	[SerializeField] private GameObject soundEffect;

	//do enemies take damage from this
	[SerializeField] private bool damagesEnemies;

    //idle state for enemies
    public override void Idle()
	{
        //disable line renderer
        eyeLaser.enabled = false;

		//the distance to the player
		float distance = Vector3.Distance (transform.position, player.transform.position);

		//if the player is close to the enemy
		if(distance <= maxDetectionDistance)
		{
			//used to check if the enemy has a line of sight on the player
			RaycastHit hit;

			//the direction from the enemy to the player
			Vector3 directionToPlayer = player.transform.position - transform.position;

			//can the enemy see the player
			if (Physics.Raycast (transform.position, directionToPlayer, out hit, maxDetectionDistance))
			{
				//yes then change to the move state
				enemyState = state.logic;
			}
		}
	}

	//moves the enemy
	public override void MoveCloser()
	{
		//set the player as the destination
		agent.SetDestination (player.transform.position);
		//allow the enemy to move
		agent.isStopped = false;
		//return to ai logic
		enemyState = state.logic;
	}

	//moves the enemy
	public override void MoveAway()
	{
		//direction to travel
		Vector3 heading = transform.position - player.transform.position;
		//desired position
		Vector3 destination = heading + transform.position;
		//set a destination for our ai
		agent.SetDestination (destination);
		//allow the enemy to move
		agent.isStopped = false;
		//return to ai logic
		enemyState = state.logic;
	}

	//selects a random point on a navmesh and sets the destination
	Vector3 RandomNavmeshLocation()
	{
        //disable line renderer
        eyeLaser.enabled = false;

        float radius = (Random.value * Random.Range (3, 7));

		//get a random direction
		Vector3 randomDirection = (Random.insideUnitSphere * radius) + transform.position ;

		//used to check for a hit
		NavMeshHit hit;
		//if we have a 
		NavMesh.SamplePosition(randomDirection, out hit, radius, 1);

		return hit.position;
	}

    //overide death to play particles
    public override void Death()
    {
        Instantiate(deathOb, transform.position, Quaternion.identity);
		//BaseDeathParticle.Play();
		//Debug.Log("I should be dying");
		//Invoke("DelayedDeath", 0.9f);
		Destroy(this.gameObject);

	}

    public override void Logic()
	{
		//where we store our hit information
		RaycastHit hit;
		//get the direction to the player
		Vector3 directionToPlayer = player.transform.position - transform.position;

		//can we see the player
		if (Physics.Raycast (transform.position, directionToPlayer, out hit, maxDetectionDistance))
		{
			//the distance to the player
			float distance = Vector3.Distance (transform.position, player.transform.position);

			//are we out of range
			if(distance > shootRange)
			{
				//move closer
				enemyState = state.moveCloser;
			}
			//are we within shoot range
			else if(distance > beamRange && distance <= shootRange)
			{
				//has the beam cooldown finished
				if(Time.time >= beamCooldown)
				{
					//is our health above half
					if(health > (maxHealth / 2))
					{
						//move closer
						enemyState = state.moveCloser;
					}
					//is our health below half
					else if(health < (maxHealth / 2))
					{
						//shoot
						enemyState = state.attackOne;
					}
				}
				//the beam cooldown hasnt finished
				else if(Time.time < beamCooldown)
				{
					//shoot
					enemyState = state.attackOne;
				}
			}
			//are we within beam range
			else if(distance > explodeRange && distance <= beamRange)
			{
				//is our health above half
				if(health > (maxHealth / 2))
				{
					//has the beam cooldown finished
					if(Time.time >= beamCooldown)
					{

						enemyState = state.attackTwo;
					}
					//the beam cooldown hasnt finished
					else if(Time.time < beamCooldown)
					{
						//move away from the player
						enemyState = state.moveAway;
					}
				}
				//is our health below half
				else if(health < (maxHealth / 2))
				{
					//move away from the player
					enemyState = state.moveAway;
				}
			}
			//are we within explode range
			else if(distance <= explodeRange)
			{
				//is our health below half
				if(health < (maxHealth / 2))
				{
					//expolde
					enemyState = state.attackThree;
				}
				//is our health above half
				else if(health > (maxHealth / 2))
				{
					//move away from the player
					enemyState = state.moveAway;
				}
			}
		}
		else
		{
			//we saw the player and have lost sight of them, move to the player
			enemyState = state.moveCloser;
		}
	}

	//first attack type
	public override void AttackOne()
	{
        //disable line renderer
        eyeLaser.enabled = false;
        //stop the enemy from moving
        agent.isStopped = true;

		//used to check if the enemy has a line of sight on the player
		RaycastHit hit;

		//the direction from the enemy to the player
		Vector3 directionToPlayer = player.transform.position - transform.position;

		//can the enemy see the player
		if (Physics.Raycast (transform.position, directionToPlayer, out hit, maxDetectionDistance)) 
		{
			//if the cooldown has finished
			if(Time.time >  shootCooldown)
			{
				transform.LookAt(player.transform);

				//reset the cooldown
				shootCooldown = Time.time + shootCooldownTime;

                //deal damage to the player
                //Instantiate(littleLaser, startPoint.transform.position, Quaternion.identity);
				playerControllerScript.DealDamage (damageAttackOne);
				enemyAS.PlayOneShot(attackOneAC);
			}
		}

		//allows the enmy to move
		agent.isStopped = false;
		//return to ai logic
		enemyState = state.logic;
	}

	//second attack type
	public override void AttackTwo()
	{
		//if the cooldown for this attack has finished
		if(Time.time > beamCooldown)
		{
            enemyAnimator.StopPlayback();
			enemyAnimator.Play("beam");
			//stops the enemy from moving
			agent.isStopped = true;
			//reset the cooldown
			beamCooldown = Time.time + beamCooldownTime;

			//look at the player
			target = shootPoint.transform.position;

			//turn on the hitbox
			Invoke ("BeamAttackActive", beamWindUpTime);
			enemyAS.PlayOneShot(attackTwoAC);
		}
	}

	//turns on the beam hitbox
	void BeamAttackActive()
	{
        
		//look at the target position
		transform.LookAt (target);
		//turn the hitbox on
		beamHitbox.SetActive (true);
		//reset the attack
		Invoke ("BeamAttackInactive", beamDuration);

        //turn on line renderer
        eyeLaser.enabled = true;
        //shoot laser renderer at points
        eyeLaser.SetPosition(0, startPoint.transform.position);
        eyeLaser.SetPosition(1, endPoint.transform.position);
        //activate particle systems
        BallEnd.Play();
        BallStart.Play();
        Flash.Play();
    }

	//turns off the beam hitbox
	void BeamAttackInactive()
	{
        eyeLaser.enabled = false;
        enemyAnimator.Play("idle");
		//turn off the hitbox
		beamHitbox.SetActive (false);

		//allows this enemy to move again
		agent.isStopped = false;
		//return to ai logic
		enemyState = state.logic;
	}

	//third attack type
	public override void AttackThree()
	{
		enemyAnimator.StopPlayback();
		enemyAnimator.Play("death");
		
		//stops this enemy from moving
		agent.isStopped = true;

		//trigger the self destruct
		Invoke ("Explode", explodeTime);
	}

	//this enemy explodes
	void Explode()
	{
        Instantiate(explodeOb, transform.position, Quaternion.identity);
        //does this move dmage enemies?
        if (damagesEnemies == true)
		{
			//creates an array of colliders and fills it with everything within the explosion radius
			Collider[] hitColliders = Physics.OverlapSphere (this.gameObject.transform.position, explodeRange);
           

			//for every collider that we found
			foreach (var item in hitColliders) 
			{
				//check what types of enemy it was
				if(item.gameObject.tag == "EnemyEyebot")
				{
					//deal damage to the enemy
					item.GetComponent<EnemyEyebot> ().health -= damageAttackThree;
				}
				else if(item.gameObject.tag == "EnemyPig")
				{
					//deal damage to the enemy
					item.GetComponent<EnemyPig> ().health -= damageAttackThree;
				}
				else if(item.gameObject.tag == "EnemySassySama")
				{
					//deal damage to the enemy
					item.GetComponent<EnemySassySama> ().health -= damageAttackThree;
				}
			}
		}

		Vector3 directionToPlayer = player.transform.position - transform.position;
		RaycastHit hit;

		//check for los as well dumbass
		if (Vector3.Distance(player.transform.position, transform.position) < explodeRange && 
			Physics.Raycast(transform.position, directionToPlayer, out hit, maxDetectionDistance))
		{
			//deal damage to player
			playerControllerScript.DealDamage (damageAttackThree);
		}

		Instantiate(soundEffect, transform.position, Quaternion.identity);

        //destroy this enemy
        Invoke("DelayedDeath", 1);
	}

    private void DelayedDeath()
    {
        Destroy(this.gameObject);
    }
}