﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

//Author/s:Bryce Prince
//Date:23/06/18
//Function:hold beaviours for the pig enemy
public class EnemyPig : EnemyAI 
{
	[Header("Pierce Settings")]
	[SerializeField] private float pierceRange;
	[SerializeField] private float pierceCooldownTime;
	private float pierceCooldown;
	public int damageAttackOne;
	[SerializeField] private float pierceWindUpTime;
	[SerializeField] private float pierceDuration;
	[SerializeField] private GameObject pierceHitbox;

	[Header("Charge Settings")]
	[SerializeField] private float chargeRange;
	[SerializeField] private float chargeCooldownTime;
	private float chargeCooldown;
	public int damageAttackTwo;
	[SerializeField] private float chargeWindUpTime;
	[SerializeField] private float chargeDuration;
	[SerializeField] private GameObject chargeHitbox;
	private Vector3 target;

	[HideInInspector] public int damageAttackThree;

    [SerializeField] private GameObject pigDeathParticle;

	//idle state for enemies
	public override void Idle()
	{
		//the distance to the player
		float distance = Vector3.Distance (transform.position, player.transform.position);

		//if the player is close to the enemy
		if(distance <= maxDetectionDistance)
		{
			//used to check if the enemy has a line of sight on the player
			RaycastHit hit;

			//the direction from the enemy to the player
			Vector3 directionToPlayer = player.transform.position - transform.position;

			//can the enemy see the player
			if (Physics.Raycast (transform.position, directionToPlayer, out hit, maxDetectionDistance))
			{
				//yes then change to the move state
				enemyState = state.logic;
			}
		}
	}

    public override void Death()
    {
        Instantiate(pigDeathParticle, transform.position, Quaternion.identity);
        base.Death();
    }

    //moves the enemy
    public override void MoveCloser()
	{
		agent.SetDestination (player.transform.position);
		agent.isStopped = false;

		//get the distance to the player
		float distance = Vector3.Distance (player.transform.position, transform.position);

		//if we are out of range
		if(distance > chargeRange)
		{
			//move closer to the player
			enemyState = state.moveCloser;
		}
		//within charge range
		else if(distance > pierceRange && distance <= chargeRange)
		{
			//is charge available
			if(Time.time >= chargeCooldown)
			{
				//charrge at the player
				enemyState = state.attackTwo;
			}
			//charge isnt available
			else if(Time.time < chargeCooldown)
			{
				//move closer to the player
				enemyState = state.moveCloser;
			}
		}
		//within pierce range
		else if(distance < pierceRange)
		{
			//is charge available
			if(Time.time >= chargeCooldown)
			{
				//move away from the player
				enemyState = state.moveAway;
			}
			//charge isnt available
			else if(Time.time < chargeCooldown)
			{
				//if pierce is avalable
				if(Time.time >= pierceCooldown)
				{
					//headbutt the player
					enemyState = state.attackOne;
				}
				//if pierce isnt avalable
				else if(Time.time >= pierceCooldown)
				{
					//headbutt the player
					enemyState = state.moveAway;
				}
			}
		}
	}

	//moves the enemy
	public override void MoveAway()
	{
		agent.SetDestination (RandomNavmeshLocation());
		agent.isStopped = false;
	}

	//selects a random point on a navmesh and sets the destination
	Vector3 RandomNavmeshLocation()
	{
		float radius = (Random.value * Random.Range (3, 7));

		Vector3 directionFromPlayer = transform.position - player.transform.position;

		Vector3 direction = directionFromPlayer + transform.position;

		//get a random direction
		Vector3 randomDirection = (Random.insideUnitSphere * radius) + direction ;

		//used to check for a hit
		NavMeshHit hit;
		//if we have a 
		NavMesh.SamplePosition(randomDirection, out hit, radius, 1);

		return hit.position;
	}

	//ai logic
	public override void Logic()
	{
		//stop the enemy from moving
		agent.isStopped = true;

		//where we stor our hit information
		RaycastHit hit;
		//the direction to the player
		Vector3 heading = player.transform.position - transform.position;
		//if this enemy can see the player
		if(Physics.Raycast(transform.position, heading, out hit, maxDetectionDistance))
		{
			//get the distance to the player
			float distance = Vector3.Distance (player.transform.position, transform.position);

			//if we are out of range
			if(distance > chargeRange)
			{
				//move closer to the player
				enemyState = state.moveCloser;
			}
			//within charge range
			else if(distance > pierceRange && distance <= chargeRange)
			{
				//is charge available
				if(Time.time >= chargeCooldown)
				{
					//charrge at the player
					enemyState = state.attackTwo;
				}
				//charge isnt available
				else if(Time.time < chargeCooldown)
				{
					//move closer to the player
					enemyState = state.moveCloser;
				}
			}
			//within pierce range
			else if(distance < pierceRange)
			{
				//is charge available
				if(Time.time >= chargeCooldown)
				{
					//move away from the player
					enemyState = state.moveAway;
				}
				//charge isnt available
				else if(Time.time < chargeCooldown)
				{
					//if pierce is avalable
					if(Time.time >= pierceCooldown)
					{
						//headbutt the player
						enemyState = state.attackOne;
					}
					//if pierce isnt avalable
					else if(Time.time >= pierceCooldown)
					{
						//headbutt the player
						enemyState = state.moveAway;
					}
				}
			}
		}
		else
		{
			//we saw the player and have lost sight of them, move to the player
			enemyState = state.moveCloser;
		}
	}

	//first attack type
	public override void AttackOne()
	{
		//if the cooldown has finished
		if(Time.time > pierceCooldown)
		{
			ResetVelocity();
			//reset the cooldown
			pierceCooldown = Time.time + pierceCooldownTime;
			//stop the enemy from moving
			agent.isStopped = true;
			enemyAnimator.Play("chargePrep");
			//start the attack
			Invoke ("PierceActive", pierceWindUpTime);
		}
	}

	//punch attack start
	void PierceActive()
	{
		//look at the player
		transform.LookAt (player.transform);
		//turn on the hitbox
		pierceHitbox.SetActive (true);
		//reset the attack
		enemyAS.PlayOneShot(attackOneAC, 0.2f);
		Invoke ("PierceInactive", pierceDuration);
	}

	//punch attack reset
	void PierceInactive()
	{
		ResetVelocity();
		//turn off the hitbox
		pierceHitbox.SetActive (false);
		//allow the enemy to move
		agent.isStopped = false;
		//return to the decision making logic
		enemyState = state.logic;
	}

	//second attack type
	public override void AttackTwo()
	{
		ResetVelocity();
		//used to check if the enemy has a line of sight on the player
		RaycastHit hit;

		//the direction from the enemy to the player
		Vector3 directionToPlayer = player.transform.position - transform.position;
		//can the enemy see the player
		if (Physics.Raycast (transform.position, directionToPlayer, out hit, maxDetectionDistance))
		{
			//if tthe cooldown has
			if(Time.time > chargeCooldown)
			{
				chargeCooldown = Time.time + chargeCooldownTime;

				agent.isStopped = true;

				target = player.transform.position;
				enemyAnimator.Play("chargePrep");
				enemyAS.PlayOneShot(attackTwoAC, 0.15f);
				Invoke("ChargeActive", chargeWindUpTime);
			}
		}
	}

	//charge attack start
	void ChargeActive()
	{
		transform.LookAt(player.transform);

		//activeate the hitbox
		chargeHitbox.SetActive(true);
		//move the enemy
		StartCoroutine ("Charge");
		//reset the attack
		Invoke("ChargeInactive", chargeDuration);
	}

	//moves the enemy to the target
	IEnumerator Charge()
	{
		//the distance the enemy needs to travel
		float distance = Vector3.Distance (transform.position, target);
		//the speed the enemy should move
		float speed = distance / chargeDuration;

		//repeats until the enemy has reached the target
		for (float i = 0; i < distance; i--)
		{
			//move the enemy towards the target in small steps
			transform.position = Vector3.MoveTowards (transform.position, target, speed * Time.deltaTime);
			//we dont need to return anything
			yield return null;
		}
	}

	//charge attack reset
	void ChargeInactive()
	{
		ResetVelocity();
		//turns off the hitbox
		chargeHitbox.SetActive(false);
		//stops the enmey from charging
		StopCoroutine ("Charge");
		//allows the enemy to move again
		agent.isStopped = false;
		//return to the decision making logic
		enemyState = state.logic;
	}

	//third attack type
	public override void AttackThree()
	{
		
	}

	void ResetVelocity()
	{
		enemyRB.velocity = Vector3.zero;
		enemyRB.angularVelocity = Vector3.zero;

	}
}