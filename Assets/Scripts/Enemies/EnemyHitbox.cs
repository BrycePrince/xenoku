﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Author/s:Bryce Prince
//Date:24/06/18
//Function:holds functionality for hitbox based attacks
public class EnemyHitbox : MonoBehaviour 
{
	[SerializeField] private int attackType;
	private int damage;
	private GameObject player;

	//Use this for initialization
	void Start () 
	{
		player = GameObject.Find ("Player");

		if(GetComponentInParent<EnemyEyebot>() != null && attackType == 1)
		{
			damage = GetComponentInParent<EnemyEyebot> ().damageAttackOne;

		}
		else if(GetComponentInParent<EnemyEyebot>() != null && attackType == 2)
		{
			damage = GetComponentInParent<EnemyEyebot> ().damageAttackTwo;

		}
		else if(GetComponentInParent<EnemyEyebot>() != null && attackType == 3)
		{
			damage = GetComponentInParent<EnemyEyebot> ().damageAttackThree;

		}

		if(GetComponentInParent<EnemyPig>() != null && attackType == 1)
		{
			damage = GetComponentInParent<EnemyPig> ().damageAttackOne;

		}
		else if(GetComponentInParent<EnemyPig>() != null && attackType == 2)
		{
			damage = GetComponentInParent<EnemyPig> ().damageAttackTwo;

		}
		else if(GetComponentInParent<EnemyPig>() != null && attackType == 3)
		{
			damage = GetComponentInParent<EnemyPig> ().damageAttackThree;

		}

		if(GetComponentInParent<EnemySassySama>() != null && attackType == 1)
		{
			damage = GetComponentInParent<EnemySassySama> ().damageAttackOne;

		}
		else if(GetComponentInParent<EnemySassySama>() != null && attackType == 2)
		{
			damage = GetComponentInParent<EnemySassySama> ().damageAttackTwo;

		}
		else if(GetComponentInParent<EnemySassySama>() != null && attackType == 3)
		{
			damage = GetComponentInParent<EnemySassySama> ().damageAttackThree;

		}
	}
	
	//Update is called once per frame
	void Update () 
	{
		
	}

	void OnCollisionEnter(Collision coll)
	{
		
	}

	void OnCollisionStay(Collision coll)
	{

	}

	void OnCollisionExit(Collision coll)
	{

	}

	void OnTriggerEnter(Collider coll)
	{
		if(coll.gameObject.tag == "Player")
		{
			//deal damage to the player
			player.GetComponentInParent<PlayerControllerScript> ().DealDamage (damage);
		}
	}

	void OnTriggerStay(Collider coll)
	{
		if(coll.gameObject.tag == "Player")
		{
			//deal damage to the player
			player.GetComponentInParent<PlayerControllerScript> ().DealDamage (damage);
		}
	}

	void OnTriggerExit(Collider coll)
	{

	}
}