using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Author/s:Bryce Prince
//Date:23/06/18
//Function:hold beaviours for the third enemy
public class EnemySassySama : EnemyAI 
{
	[Header("Universal settings")]
	[SerializeField] private float attackRange;

	[Header("Punch Settings")]
	[SerializeField] private float punchCooldownTime;
	private float punchCooldown;
	public int damageAttackOne;
	[SerializeField] private float punchWindUpTime;
	[SerializeField] private float punchDuration;
	[SerializeField] private GameObject punchHitbox;

	[Header("Burn Settings")]
	[SerializeField] private float burnCooldownTime;
	private float burnCooldown;
	public int damageAttackTwo;
	[SerializeField] private float burnWindUpTime;
	[SerializeField] private float burnDuration;
	[SerializeField] private GameObject burnHitbox;

	[Header("Flame Punch Settings")]
	[SerializeField] private float flamePunchCooldownTime;
	private float flamePunchCooldown;
	public int damageAttackThree;
	[SerializeField] private float flamePunchWindUpTime;
	[SerializeField] private float flamePunchDuration;
	[SerializeField] private GameObject flamePunchHitbox;

	//idle state for enemies
	public override void Idle()
	{
		//the distance to the player
		float distance = Vector3.Distance (transform.position, player.transform.position);

		//if the player is close to the enemy
		if(distance <= maxDetectionDistance)
		{
			//used to check if the enemy has a line of sight on the player
			RaycastHit hit;

			//the direction from the enemy to the player
			Vector3 directionToPlayer = player.transform.position - transform.position;

			//can the enemy see the player
			if (Physics.Raycast (transform.position, directionToPlayer, out hit, maxDetectionDistance))
			{
				//yes then change to the move state
				enemyState = state.logic;
			}
		}
	}

	public override void Logic()
	{
		//where we stor our hit information
		RaycastHit hit;
		//the direction to the player
		Vector3 heading = player.transform.position - transform.position;
		//if this enemy can see the player
		if(Physics.Raycast(transform.position, heading, out hit, maxDetectionDistance))
		{
			//get the distance to the player
			float distance = Vector3.Distance (player.transform.position, transform.position);

			//if the player is out of range
			if(distance > attackRange)
			{
				//move closer to the player
				enemyState = state.moveCloser;
			}
			//if the play is in range
			else if(distance <= attackRange)
			{
				//has the cooldown for "flame punch" finished
				if(Time.time >= flamePunchCooldown)
				{
					//attack the player with the "flame punch" move
					enemyState = state.attackThree;
				}
				//if it hasn't
				else if(Time.time < flamePunchCooldown)
				{
					//has the cooldown for "burn" finished
					if(Time.time >= burnCooldown)
					{
						//attack the player with the "burn" move
						enemyState = state.attackTwo;
					}
					//if it hasn't
					else if(Time.time < burnCooldown)
					{
						//attack the player with the "punch" move
						enemyState = state.attackOne;
					}
				}
			}
		}
		else
		{
			//we saw the player and have lost sight of them, move to the player
			enemyState = state.moveCloser;
		}
	}

	//moves the enemy
	public override void MoveCloser()
	{
		//move to the player
		agent.SetDestination (player.transform.position);

		//get the distance to the player
		float distance = Vector3.Distance (player.transform.position, transform.position);

		//are we in range
		if(distance <= attackRange)
		{
			//return to decision making logic
			enemyState = state.logic;
		}
	}

	//moves the enemy
	public override void MoveAway()
	{

	}

	//first attack type
	public override void AttackOne()
	{
		//if the cooldown has finished
		if(Time.time > punchCooldown)
		{
			//reset the cooldown
			punchCooldown = Time.time + punchCooldownTime;
			//stop the enemy from moving
			agent.isStopped = true;
			//start the attack
			Invoke ("PunchActive", punchWindUpTime);
		}
	}

	//punch attack start
	void PunchActive()
	{
		//look at the player
		transform.LookAt (player.transform);
		//turn on the hitbox
		punchHitbox.SetActive (true);
		//reset the attack
		Invoke ("PunchInactive", punchDuration);
	}

	//punch attack reset
	void PunchInactive()
	{
		//turn off the hitbox
		punchHitbox.SetActive (false);
		//allow the enemy to move
		agent.isStopped = false;
		//return to the decision making logic
		enemyState = state.logic;
	}

	//second attack type
	public override void AttackTwo()
	{
		//if the cooldown has finished
		if(Time.time > burnCooldown)
		{
			//reset the cooldown
			burnCooldown = Time.time + burnCooldownTime;
			//stop the enemy from moving
			agent.isStopped = true;
			//start the attack
			Invoke ("BurnActive", burnWindUpTime);
		}
	}

	//burn attack start
	void BurnActive()
	{
		//look at the player
		transform.LookAt (player.transform);
		//turn on the hitbox
		burnHitbox.SetActive (true);
		//rreset the attack
		Invoke ("BurnInactive", burnDuration);
	}

	//burn attack reset
	void BurnInactive()
	{
		//turn off the hitbox
		burnHitbox.SetActive (false);
		//allow the enemy to move
		agent.isStopped = false;
		//return to the decision making logic
		enemyState = state.logic;
	}

	//second attack type
	public override void AttackThree()
	{
		//if the cooldown has finished
		if(Time.time > flamePunchCooldown)
		{
			//reset the cooldown
			flamePunchCooldown = Time.time + flamePunchCooldownTime;
			//stops the enemy from moving
			agent.isStopped = true;
			//starts the attack
			Invoke ("FlamePunchActive", flamePunchWindUpTime);
		}
	}

	// attack start
	void FlamePunchActive()
	{
		//look at the player
		transform.LookAt (player.transform);
		//turn on the hitbox
		flamePunchHitbox.SetActive (true);
		//reset the attack
		Invoke ("FlamePunchInactive", flamePunchDuration);
	}

	// attack reset
	void FlamePunchInactive()
	{
		//turn off the hitbox
		flamePunchHitbox.SetActive (false);
		//allow the enemy to move
		agent.isStopped = false;
		//return to the decision making logic
		enemyState = state.logic;
	}
}