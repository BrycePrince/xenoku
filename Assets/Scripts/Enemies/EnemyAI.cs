﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

//Author/s:Bryce Prince
//Date:23/06/18
//Function:holds behaviours for enemies
public class EnemyAI : MonoBehaviour 
{
	[HideInInspector] public Rigidbody enemyRB;
	[HideInInspector] public Animator enemyAnimator;
	[HideInInspector] public GameObject player;
    [HideInInspector] public GameObject shootPoint;
    [HideInInspector] public PlayerControllerScript playerControllerScript;
	[HideInInspector] public NavMeshAgent agent;
	public enum state 
	{
		idle,
		moveCloser,
		moveAway,
		logic,
		attackOne,
		attackTwo,
		attackThree
	};
	public state enemyState = state.idle;
	public bool startAtMaxHealth;
	public int maxHealth;
	public int health;
	public float maxDetectionDistance;
	[HideInInspector] public AudioSource enemyAS;
	public AudioClip attackOneAC;
	public AudioClip attackTwoAC;
	public AudioClip attackThreeAC;

	[Header("Laser Settings")]
	public float laserCooldownTime;
	private float laserCooldown;
	public int laserDamage;
    

	//Use this for initialization
	void Start () 
	{
		enemyAS = GetComponent<AudioSource>();
		enemyAnimator = GetComponent<Animator>();
		//find the player
		player = GameObject.Find ("Player");
        shootPoint = GameObject.Find("ShootPoint");
		//find the player controller
		playerControllerScript = player.GetComponentInParent<PlayerControllerScript> ();
		//find the navMeshAgent
		agent = GetComponent<NavMeshAgent> ();
		enemyRB = GetComponent<Rigidbody>();

		//does this enemy start with full health
		if(startAtMaxHealth == true)
		{
			//set health to maximum
			health = maxHealth;
		}
	}
	
	//Update is called once per frame
	void Update () 
	{
		//if the is enemy has 0 or less health
		if(health < 1)
		{
            //destroy the enemy
            //Destroy (this.gameObject);
            Death();
		}

		//controls which mode the enemies are in
		switch (enemyState) 
		{
		//idle mode
		case state.idle:
			Idle ();
			break;

			//movement mode
		case state.moveCloser:
			MoveCloser ();
			break;

			//movement mode
		case state.moveAway:
			MoveAway ();
			break;

			//movement mode
		case state.logic:
			Logic ();
			break;

			//attack one
		case state.attackOne:
			AttackOne ();
			break;

			//attack one
		case state.attackTwo:
			AttackTwo ();
			break;

			//attack one
		case state.attackThree:
			AttackThree ();
			break;
		}
	}

	//deals damage to the enemy
	public void DealDamage(int damageToDeal)
	{
		//deals damage to the enemy
		health -= damageToDeal;
	}

	//idle state for enemies
	public virtual void Idle()
	{
		
	}

	//moves the enemy
	public virtual void MoveCloser()
	{
		
	}

	//moves the enemy
	public virtual void MoveAway()
	{

	}

	//ai logic
	public virtual void Logic()
	{

	}

	//first attack type
	public virtual void AttackOne()
	{
		
	}

	//second attack type
	public virtual void AttackTwo()
	{

	}

	//third attack type
	public virtual void AttackThree()
	{

	}

    //death particles
    public virtual void Death()
    {
        Destroy(this.gameObject);
    }

	//did we hit something
	void OnCollisionEnter(Collision coll)
	{
		//was it a shotgun bullet
		if (coll.gameObject.tag == "BulletShotGun")
		{
			//reduce health by the amount of damage dealt
			health -= coll.gameObject.GetComponent<BulletShotgunScript>().damage;
		}
		//was it an assault rifle bullet
		else if (coll.gameObject.tag == "BulletAssaultRifle")
		{
			//reduce health by the amount of damage dealt
			health -= coll.gameObject.GetComponent<BulletAssaultRifleScript>().damage;
		}
		//was it the laser
		else if (coll.gameObject.tag == "Laser")
		{
			//has the cooldown finished
			if (Time.time > laserCooldown)
			{
				//deal damage
				health -= laserDamage;
				//apply the cooldown time
				laserCooldown = Time.time + laserCooldownTime;
			}
		}
	}

	//are we still hitting something
	void OnCollisionStay(Collision coll)
	{
		//was it the laser
		if (coll.gameObject.tag == "Laser")
		{
			//has the cooldown finished
			if (Time.time > laserCooldown)
			{
				//deal damage
				health -= laserDamage;
				//apply the cooldown time
				laserCooldown = Time.time + laserCooldownTime;
			}
		}
	}

	//did we stop hitting something
	void OnCollisionExit(Collision coll)
	{

	}

	//did we hit a trigger
	void OnTriggerEnter(Collider coll)
	{
		
	}

	//are we still hitting a trigger
	void OnTriggerStay(Collider coll)
	{
		
	}

	//did we stop hitting a trigger
	void OnTriggerExit(Collider coll)
	{

	}
}