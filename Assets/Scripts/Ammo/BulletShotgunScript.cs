﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Author/s:Bryce Prince
//Date:30/4/18
//Function:holds properties for shotgun bullets
public class BulletShotgunScript : BulletScript 
{
	private void Start()
	{
        Invoke("DestroySelf", destroyTime);
	}

	// Update is called once per frame
	void Update () 
	{
		//make the bullet move at its unique speed
		MoveForward (bulletSpeed);
	}
}