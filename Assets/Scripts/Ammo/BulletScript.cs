﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Author/s:Bryce Prince
//Date:5/4/18
//Function:holds all bullet attributes
public class BulletScript : MonoBehaviour 
{
	//the speed of the bullet
	public float bulletSpeed;
	//how long before the prefab deletes itself
	public float destroyTime;

    //Particle Effect
    [SerializeField] public GameObject impactEffect;

    public int damage;

    //destroy the object this script is attached to (shotgun and assault rifle bullets only)
    public void DestroySelf()
	{
		//destroy the object
		Destroy (this.gameObject);
	}

	//moves the bullet at its unique speed
	public void MoveForward(float speed)
	{
		//make the object move
		transform.Translate(Vector3.forward * Time.deltaTime * speed);
	}

	//did we hit something
	void OnCollisionEnter(Collision coll)
	{
		//is this object a grenade
		if(this.gameObject.tag == "BulletGrenade")
		{
			//yes, then make our velocity, spin and movespeed 0
			bulletSpeed = 0;
			Rigidbody rb = GetComponent<Rigidbody> ();
			rb.velocity = Vector3.zero;
			rb.angularVelocity = Vector3.zero;

			//parent the grenade to what we hit
			transform.SetParent (coll.gameObject.transform);
		}
		//if this object is not a grenade
		else
		{
            Instantiate(impactEffect, transform.position, Quaternion.identity);
         
			//prematurely calls the destroy function because we hit something
			DestroySelf ();
		}
	}
}