﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Author/s:Bryce Prince
//Date:30/4/18
//Function:holds properties for grenades
public class BulletGrenadeScript : BulletScript 
{
	private GameObject player;
	private PlayerControllerScript playerControllerScript;
	//how long before the grenade explodes
	[SerializeField] private float fuseTime;
	//the radius of the explosion
	[SerializeField] private float ExplosionRadius;
	[SerializeField] private GameObject soundEffect;
    [SerializeField] private GameObject grenadeParticle;
	// Use this for initialization
	void Start () 
	{
		//run the explode function after the fusetime has passed
		Invoke ("Explode", fuseTime);

		player = GameObject.Find ("Player");
		playerControllerScript = player.GetComponentInParent<PlayerControllerScript> ();
	}
	
	// Update is called once per frame
	void Update () 
	{
		//make the bullet move at its unique speed
		MoveForward (bulletSpeed);
	}

	void Explode()
	{
        Instantiate(grenadeParticle, transform.position, Quaternion.identity);
        //creates an array of colliders and fills it with everything within the explosion radius
        Collider[] hitColliders = Physics.OverlapSphere (this.gameObject.transform.position, ExplosionRadius);

		//for every collider that we found
		foreach (Collider item in hitColliders) 
		{
			//check what types of enemy it was
			if(item.gameObject.tag == "EnemyEyebot")
			{
				//deal damage to the enemy
				item.GetComponent<EnemyEyebot> ().health -= damage;
			}
			else if(item.gameObject.tag == "EnemyPig")
			{
				//deal damage to the enemy
				item.GetComponent<EnemyPig> ().health -= damage;
			}
			else if(item.gameObject.tag == "EnemySassySama")
			{
				//deal damage to the enemy
				item.GetComponent<EnemySassySama> ().health -= damage;
			}
		}

		if(Vector3.Distance(player.transform.position, transform.position) < ExplosionRadius)
		{
			//deal damage to player
			playerControllerScript.DealDamage (damage);
		}

        
        Instantiate(soundEffect, transform.position, Quaternion.identity);
        
		Destroy(this.gameObject);
	}
}