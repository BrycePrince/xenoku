﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Author/s:Bryce Prince
//Date:20/05/18
//Function:spawns objects in predefined locations upon trigger
public class SpawnerScript : MonoBehaviour
{
	//makes this class visible in the editor
	[System.Serializable]
	//new class that will hold the properties for each object we want to spawn
	public class SpawnOptions
	{
		//how many of an object to spawn
		public int numberToSpawn;
		//reference to the object prefab that we want to spawn a clone of
		public GameObject objectToSpawn;
		//locations that we can spawn our object at
		public GameObject[] spawnPoints;
	}

	//manual trigger for spawning Objects.
	//instructions to access this from another script:
	//1. add "public/ private SpawnerScript spawnerScript;" in your variable declaration lines.
	//2.1 drag the object with the "SpawnerScript" attached to it into the "spawnerScript" field.
	//2.2 OR use "GameObject.Find ("NameOfGameObject");" in start.
	//3. use "spawnerScript.spawn = true/false;" at the desired point in your Script.
	public bool spawn;
	//do we destroy this object after spawning our objects
	//instructions to access this from another script:
	//repeat above instructions while replacing every reference to "spawn" with "destroyOnSpawn".
	public bool destroyOnSpawn;
	//do we want spawning to begin when something collides with this object
	//instructions to access this from another script:
	//repeat above instructions while replacing every reference to "spawn" with "spawnOnCollision".
	public bool spawnOnCollision;

	//array of SpawnOptions
	public SpawnOptions[] spawnOptions;

	// Update is called once per frame
	void Update()
	{
		//if the trigger has been activated
		if(spawn == true)
		{
			//begin spawning objects
			Spawn ();
		}
	}

	//when another gameobject with a collider hits this one
	void OnTriggerEnter(Collider coll)
	{
		//if we want a collision with this objects collider to trigger the spawning
		if(spawnOnCollision == true)
		{
			//did the other object have the tag "Player"
			if(coll.gameObject.tag == "Player")
			{
				//activate the trigger
				spawn = true;
			}
		}
	}

	//spawns objects
	void Spawn ()
	{
		//for every spawnOption in our SpawnOptions array
		foreach (SpawnOptions spawnOption in spawnOptions) 
		{
			//repeat this code for the number in numberToSpawn found in the spawnOption
			for (int i = 0; i < spawnOption.numberToSpawn; i++) 
			{
				//create an integer with a random value between 0 and the maximum number of spawnPoints in spawnOption
				int randomValue = Random.Range (0, spawnOption.spawnPoints.Length);

				//create a clone of the object at the randomly selected spawnpoint with no rotation
				Instantiate (spawnOption.objectToSpawn, spawnOption.spawnPoints[randomValue].transform.position, Quaternion.identity);
			}
		}

		//do we want to destroy this object after spawning
		if(destroyOnSpawn == true)
		{
			//destroy this object
			Destroy (this.gameObject);
		}
	}
}