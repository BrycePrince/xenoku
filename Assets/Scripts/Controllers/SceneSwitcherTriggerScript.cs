﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Author/s:Bryce Prince
//Date:01/06/18
//Function:triggers scene changes upon collision
public class SceneSwitcherTriggerScript : MonoBehaviour 
{
	[SerializeField] private string triggerTag;
	[SerializeField] private int sceneToSwitch;
	private SceneSwitcherScript sceneSwitcherScript;

	// Use this for initialization
	void Start () 
	{
		//find the sceneSwitcherScript in this scene
		sceneSwitcherScript = GameObject.Find ("Scene Switcher").GetComponent<SceneSwitcherScript> ();
	}

	//did we hit something
	void OnTriggerEnter(Collider coll)
	{
		//was it the object that triggers a scene switch
		if(coll.gameObject.tag == triggerTag)
		{
			//manual scene switch trigger
			sceneSwitcherScript.sceneSwitches [sceneToSwitch-1].switchScene = true;
		}
	}
}