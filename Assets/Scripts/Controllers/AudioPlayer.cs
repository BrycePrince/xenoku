﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioPlayer : MonoBehaviour
{
	[SerializeField] private AudioSource soundClip;

	// Use this for initialization
	void Start ()
	{
		soundClip = GetComponent<AudioSource>();
	}

	private void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.tag == "Player")
		{
			soundClip.Play();

			Destroy(this.gameObject.GetComponent<Collider>());
		}
	}
}
