using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Author/s:Bryce Prince
//Date:5/1/18
//Function:this script is a controller for a smart camera.

//instructions.
//attach this script to a camera object.
//select mode mobile, stationary, cinematic.

//Mobile 
//use decimal or whole values to set a locations in the offsetMobile X,Y and Z boxes in the editor.
//if you want the camera to face the same direction as the target when it rotates set the "mimicTargetRotationMobile" to true"
//if you want the camera to face a sngle direction then rotate it with the "rotation offset" field and keep "mimicTargetRotationMobile" off.
//drag an object into the targetMobile box.
//use a decimal or whole value to set a time delay in the "smoothTime" field.

//Stationary
//if you want to use a GameObject as a target then check the "useTargetStationary" field and drag an object into the "targetStationary" field.
//if you dont want to use a GameObject as a target then fill in the X,Y and Z fields of offsetStationary and targetPointStationary to specify the position and the point the camera should look at respectively.

//Cinematic
//"currentWaypoint" shows you which target you are moving to next.
//set loopCinematic to be true if you want the camera to go through the waypoints continuously.
//use decimal or whole values to set a movement speed in the "moveSpeedCinematic" field.
//if you want the camera to look at a GameObject then check "useTargetCinematic" and drag an object into the "targetCinematic" field.
//under the "waypoints" field set the size in whole numbers to the number of points you want the camera to move through.
//drag objects into the "targets" field. the camera will move to the top object first and the bottom one last.

//Passive 
//does nothing 
//only use if you want the camera to do nothing.

//ResetCamera
//resets all the numbered values to 0 and all booleans to false.
//resets all other fields to null

public class SmartCameraControllerScript : MonoBehaviour 
{
	//possible modes
	public enum state {stationary, mobile, cinematic, passive, reset};
	//default mode if unspecified
	public state cameraMode = state.passive;
	//velocity of camera, we can keep this private because we don't need to see or edit it.
	private Vector3 velocity;


	[Header("Mobile Mode Settings")]
	//for making the camera use the same rotation as the target
	public bool mimicTargetRotationMobile;
	//lag time for camera movement.
	public float smoothTime;
	//the location of the thing we are looking at.
	public Transform targetMobile;
	//offset for rotation
	public Vector3 rotationOffsetMobile; 
	//the distance from object.
	public Vector3 offsetMobile; 


	[Header("Stationary Mode Options")]

	//use an object as the target
	public bool useTargetStationary;
	//the object we want to target
	public Transform targetStationary;
	//the position we want the camera to be
	public Vector3 offsetStationary;
	public Vector3 targetPointStationary;


	[Header("Cinematic Mode Options")]

	//the current waypoint
	public int currentWaypoint;
	//speed of the camera for cinematic mode
	public float moveSpeedCinematic;
	//for making the camera loop through waypoints
	public bool loopCinematic;
	//are we using an object as the target
	public bool useTargetCinematic;
	//the object that is our target
	public Transform targetCinematic;
	//the locations of the waypoints
	public Transform[] waypoints;

	//this function runs once per frame.
	void Update ()
	{
		//which mode is the camera set to
		switch (cameraMode) 
		{	
		case state.stationary:

			StationaryCamera ();
			break;

		case state.mobile:

			Mobile ();
			break;

		case state.cinematic:

			CinematicCamera ();
			break;

		case state.passive:
			PassiveCamera ();

			break;

		case state.reset:
			ResetCamera ();

			break;
		}
	}

	//this function smooths the camera's movement when following a target.
	void Mobile()
	{
		//make the camera move to the desired spot with a time delay.
		transform.position = Vector3.SmoothDamp (transform.position, (targetMobile.transform.position + offsetMobile), ref velocity, smoothTime);

		if(mimicTargetRotationMobile == true)
		{
			//apply the target's rotation to the camera.
			transform.rotation = targetMobile.rotation;
		}
		else if(mimicTargetRotationMobile == false)
		{
			//override the current rotation with a user defined one.
			transform.eulerAngles = rotationOffsetMobile;
		}
	}

	//this function makes the camera look at the target, but not move.
	void StationaryCamera()
	{
		//moves the camera to the coordinates defined by the offset vector3.
		transform.position = offsetStationary;

		if(useTargetStationary == true)
		{
			//point the camera at the target.
			transform.LookAt (targetStationary);
		}
		else if(useTargetStationary == false)
		{
			//point the camera at the target.
			transform.LookAt (targetPointStationary);
		}

	}

	//this fuction makes the camera move to waypoints and (optionally) look at a target.
	void CinematicCamera() 
	{
		//are we at the last target
		if(currentWaypoint == waypoints.Length)
		{
			//yes
			//does the user want the camera to loop
			if(loopCinematic == true)
			{
				//yes, reset the current target
				currentWaypoint = 0;
			}
			else
			{
				//no, do nothing
				cameraMode = state.passive;
			}
		}
		else 
		{
			//no
			//has the camera reached the waypoint
			if(transform.position == waypoints[currentWaypoint].position)
			{
				//yes, look for another waypoint
				currentWaypoint++;
			}
			else
			{
				//no, then move the camera
				transform.position = Vector3.MoveTowards (transform.position, waypoints[currentWaypoint].position, moveSpeedCinematic * Time.deltaTime);

				//are we pointing the camera at an object
				if(useTargetCinematic == true)
				{
					//yes, point the camera at the target.
					transform.LookAt (targetCinematic);
				}
				else if(useTargetCinematic == false)
				{
					//no, do nothing

				}
			}
		}
	}

	//does nothing
	void PassiveCamera ()
	{

	}

	//reset all variables
	void ResetCamera()
	{
		mimicTargetRotationMobile 	= false;
		loopCinematic 				= false;
		useTargetCinematic 			= false;
		useTargetStationary 		= false;

		currentWaypoint 			= 0;

		smoothTime 					= 0f;
		moveSpeedCinematic 			= 0f;

		rotationOffsetMobile		= new Vector3 (0, 0, 0);
		offsetMobile	  			= new Vector3 (0, 0, 0);
		offsetStationary 			= new Vector3 (0, 0, 0);
		targetPointStationary 		= new Vector3 (0, 0, 0);
		velocity					= new Vector3 (0, 0, 0);

		targetMobile 				= null;
		targetStationary 			= null;
		targetCinematic 			= null;

		foreach (var item in waypoints) 
		{
			System.Array.Clear (waypoints, 0, waypoints.Length);

		}
	}
}