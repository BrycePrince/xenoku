﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

//Author/s:Bryce Prince
//Date:28/05/18
//Function:Switches scenes on collision or other trigger
public class SceneSwitcherScript : MonoBehaviour 
{
	public static SceneSwitcherScript instance;
	//makes this class visible in the editor
	[System.Serializable]
	//new class to store our variables
	public class SceneSwitch 
	{
		public string sceneName;
		public bool switchScene;
	}

	public SceneSwitch[] sceneSwitches;

	void Awake () {
		if (instance == null) {
			instance = this;
		} else {
			Destroy(this);
		}
	}
	//this function runs once per frame.
	void Update ()
	{
		//for each of the sceneSwitchers in our array
		foreach (SceneSwitch sceneSwitch in sceneSwitches) 
		{
			//check if the manual trigger has been activated
			if (sceneSwitch.switchScene == true)
			{
				//start switching scenes
				SwitchScene (sceneSwitch.sceneName);
			}
		}
	}

	//switches the scene
	void SwitchScene(string stringToCheck)
	{
		//if the sceneName field is not empty
		if(stringToCheck != null)
		{
			//load the desired scene
			SceneManager.LoadScene(stringToCheck);
		}
		else
		{
			//the scene name is not given in the editor
			Debug.Log("Not a Valid Scene.");
		}
	}
}