﻿using UnityEngine.Audio;
using UnityEngine;

//Author(s)/Modifier(s):Bryce Prince
//Date:19/7/18
//Function:class that store audio clip properties
[System.Serializable]
public class Sound 
{
	public string name;
	public AudioClip clip;
	[Range(0,1)] public float volume;
	[Range(0.1f,3)] public float pitch;
	public bool loop;
	[HideInInspector] public AudioSource source;
}