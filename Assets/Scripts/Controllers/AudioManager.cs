﻿using UnityEngine.Audio;
using System;
using UnityEngine;

//Author(s)/Modifier(s):Bryce Prince
//Date:19/7/18
//Function:Manages audio clips that play over the world
public class AudioManager : MonoBehaviour 
{
	public Sound[] sounds;

	public static AudioManager instance;

	//Runs Before Start
	void Awake () 
	{
		//is there another audioManager in this scene
		if(instance == null)
		{
			//there isnt so we will use this one
			instance = this;
		}
		//if there is
		else
		{
			//we dont need this audioManager so destroy it
			Destroy (this.gameObject);
			//prevents other code from running
			return;
		}

		//prevents this object from being destroyed when loading a new scene
		DontDestroyOnLoad (this.gameObject);

		//assing settings for each audioClip
		foreach (Sound sound in sounds) 
		{
			//create a audioSource component
			sound.source = gameObject.AddComponent<AudioSource> ();
			//assing settings from asset folder
			sound.source.clip = sound.clip;
			sound.source.volume = sound.volume;
			sound.source.pitch = sound.pitch;
			sound.source.loop = sound.loop;
		}
	}

	//plays a selected audioClip
	public void Play(string name)
	{
		//find the clip with a matching name in ourr array
		Sound soundClip = Array.Find (sounds, sound => sound.name == name);

		//if it can be found
		if(soundClip != null)
		{
			//play the clip
			soundClip.source.Play ();
		}
		//if it cant be found
		else
		{
			//send an error
			Debug.Log("SoundClip: " + soundClip.name + " not found");
		}
	}
}