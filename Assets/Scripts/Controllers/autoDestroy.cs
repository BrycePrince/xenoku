﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class autoDestroy : MonoBehaviour
{
	[SerializeField] private float destroyTime;


	// Use this for initialization
	void Start ()
	{
		Invoke("Destroy", destroyTime);
	}

	void Destroy()
	{
		Destroy(this.gameObject);
	}
}
