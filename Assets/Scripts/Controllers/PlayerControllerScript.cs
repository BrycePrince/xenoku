﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

//Author/s:Bryce Prince
//Date:30/4/18
//Function:Controls the player
public class PlayerControllerScript : MonoBehaviour 
{
	[Header("Animations")]
	[SerializeField] private Animator playerAnimator;
	[SerializeField] private float dodgeMoveDelayTime;
	[SerializeField] private float grenadeThrowTime;

	[Header("Player Attributes")]
	public int shields = 3;
	public int health = 100;
	[SerializeField] private float playerSpeed;
	[SerializeField] private float dodgeSpeed;
	[SerializeField] private float dodgeCooldownTime;
	[SerializeField] private float dodgeCooldown;
	[SerializeField] private float damageCooldown;
	[SerializeField] private float damageCooldownTime;
	public enum state {laser, shotgun, assaultRifle, grenades};
	public state WeaponTypes = state.laser;
	public bool playerStilled;
    public float targetRotation;
	[SerializeField] private bool damageInstances;

	[Header("Shotgun Attributes")]
	[SerializeField] private GameObject bulletShotgun;
	[SerializeField] private float shotgunCooldownTime;
	[SerializeField] private float degreesBetween;
	[SerializeField] private int numberOfPellets;
	private float shotgunCooldown;

	[Header("Assault Rifle Attributes")]
	[SerializeField] private GameObject bulletAssaultRifle;
	[SerializeField] private float assaultRifleCooldownTime;
	private float assaultRifleCooldown;

	[Header("Grenade Attributes")]
	[SerializeField] private GameObject bulletGrenade;
	[SerializeField] private float grenadeCooldownTime;
	private float grenadeCooldown;

    [Header("LineRenderer Attributes")]
    [SerializeField] private LineRenderer playerLineLaser;

	private UIController uiController;
	private SceneSwitcherScript sceneSwitcherScript;
	private Rigidbody playerRB;
	private Vector3 lookDirection;
	private Vector3 moveDirection;
	private GameObject laser;
	private GameObject gun;
    private GameObject endPoint;
	private bool dodging;
	private Vector3 inputDirection;
	private Vector3 dodgeDirection;

	[Header("Audio")]
	[SerializeField] private AudioClip[] jetpackSounds;
	[SerializeField] private AudioClip[] dodgeSounds;
	[SerializeField] private AudioClip[] healthUpSounds;
	[SerializeField] private AudioClip[] painSounds;
	private float painCooldown;
	[SerializeField] private AudioClip shieldUpSound;
	[SerializeField] private AudioClip shieldDownSound;
	[SerializeField] private AudioClip[] runningSounds;
	private float runningSoundCooldown;
	[SerializeField] private AudioClip[] weaponSounds;
	private AudioSource playerAS;

	[Header("ParticleSystem Attributes")]
    //laser
    [SerializeField] private ParticleSystem laserFlash;
    [SerializeField] private ParticleSystem laserBall;
    [SerializeField] private ParticleSystem laserBallEnd;
    [SerializeField] private ParticleSystem rifleFlash;
    
	// Use this for initialization
	void Start () 
	{
		//find the gameobject called laser
		laser = GameObject.Find("Laser");
    
		//find the gameobject called gun
		gun	= GameObject.Find("Gun");
        //find the gameobject called endpoint
        endPoint = GameObject.Find("EndPoint");

		//set the laser to be inactive in the scene
		laser.SetActive (false);
        playerLineLaser.enabled = false;
        //PC: Isolating for changes and cleanliness
        RetrieveComponents();
		//ensure the player can move
		playerStilled = false;

		Cursor.lockState = CursorLockMode.Locked;
	}
	
	//finds components 
	void RetrieveComponents ()
	{
		//find the animator component
		playerAnimator = GetComponent<Animator>();
		//find the RigidBody component on this object
		playerRB = GetComponent<Rigidbody> ();
		//find the uiController object in the scene
		uiController = UIController.controller;
		//find the sceneSwitcherScript in this scene
		sceneSwitcherScript = SceneSwitcherScript.instance;
		playerAS = GetComponent<AudioSource>();
	}

	// Update is called once per frame
	void Update()
	{

		Cursor.lockState = CursorLockMode.Locked;

		//check for player death
		if (health < 1)
		{
			//player is dead switch to lose screen
			sceneSwitcherScript.sceneSwitches[0].switchScene = true;
		}

		//check out of bounds health and shields
		health = Mathf.Clamp(health, 0, 100);
		shields = Mathf.Clamp(shields, 0, 3);

		//check if the laser is active
		LaserCheck();

		//check for player input
		GetInput();

		//changes weapons when the user presses a button
		switch (WeaponTypes)
		{
			case state.laser:
				uiController.UIWeapons(UIController.UIWeaponType.Laser);
				break;

			case state.shotgun:
				uiController.UIWeapons(UIController.UIWeaponType.Shot);
				break;

			case state.assaultRifle:
				uiController.UIWeapons(UIController.UIWeaponType.Assault);
				break;

			case state.grenades:
				uiController.UIWeapons(UIController.UIWeaponType.Grenade);
				break;
		}
	}

	void FixedUpdate()
	{
		if (dodging == true)
		{
			//transform.Translate(dodgeDirection.normalized * Time.deltaTime * dodgeSpeed, Space.World);
			Vector3 newPos = moveDirection * dodgeSpeed * Time.deltaTime;

			playerRB.MovePosition(newPos + transform.position);
		}

		if (Time.time > dodgeCooldown)
		{
			if (Input.GetAxisRaw("Dodge") != 0 && ((Input.GetAxisRaw("Horizontal") != 0.0) || Input.GetAxis("Vertical") != 0))
			{
				dodgeCooldown = Time.time + dodgeCooldownTime;

				inputDirection = new Vector3(-Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical"));
				Vector3 rotate = transform.TransformDirection(inputDirection);

				playerAnimator.SetFloat("xInput", 0);
				playerAnimator.SetFloat("zInput", 0);
				playerAnimator.StopPlayback();

				playerAS.PlayOneShot(dodgeSounds[Random.Range(0, dodgeSounds.Length)], 0.3f);
				playerAS.PlayOneShot(jetpackSounds[Random.Range(0, jetpackSounds.Length)], 0.3f);

				playerAnimator.SetFloat("dodgeXInput", rotate.x);
				playerAnimator.SetFloat("dodgeZInput", rotate.z);
				playerAnimator.Play("dodge");
				dodging = true;
				dodgeDirection = new Vector3(Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical"));
				Invoke("StopDodgeAnimation", dodgeMoveDelayTime);
				Invoke("StopDodging", dodgeMoveDelayTime);
			}
		}

		if (dodging == false)
		{
			inputDirection = new Vector3(-Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical"));
			Vector3 rotate = transform.TransformDirection(inputDirection);

			playerAnimator.SetFloat("xInput", rotate.x);
			playerAnimator.SetFloat("zInput", rotate.z);
			playerAnimator.SetFloat("dodgeXInput", 0);
			playerAnimator.SetFloat("dodgeZInput", 0);

			//when there is no player input
			if (Input.GetAxisRaw("Horizontal") == 0 && Input.GetAxisRaw("Vertical") == 0)
			{
				//PC: The reason your game movement is quite blunt is because you are just snapping straight to 0. 
				//I recommend that you use Vector3.MoveTowards to decrease the value incrementially. 

				//stop the player from moving
				playerRB.velocity = Vector3.zero;
				//stop the player from rotating
				playerRB.angularVelocity = Vector3.zero;

				playerAnimator.SetFloat("xInput", 0);
				playerAnimator.SetFloat("zInput", 0);
				playerAnimator.SetFloat("dodgeXInput", 0);
				playerAnimator.SetFloat("dodgeZInput", 0);
				playerAnimator.SetBool("dodge", false);
			}
			//when there is movement input
			else if (Input.GetAxisRaw("Horizontal") != 0 || Input.GetAxisRaw("Vertical") != 0)
			{
				if (playerStilled == true)
				{
					return;
				}
				else if (playerStilled == false)
				{
					//the direction the player is moving
					moveDirection = new Vector3(Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical"));

					//clamp the magnitude to prevent any unexpected results
					moveDirection.Normalize();

					//PC: Coming back to this, you should be using rigidbody for movement. 
					//move the player in that direction and account for the player's speed and framerate
					//transform.Translate(moveDirection * playerSpeed * Time.deltaTime, Space.World);

					Vector3 newPos = moveDirection * playerSpeed * Time.deltaTime;

					playerRB.MovePosition(newPos + transform.position);

					if (Time.time > runningSoundCooldown)
					{
						runningSoundCooldown += 0.4f;
						playerAS.PlayOneShot(runningSounds[Random.Range(0, runningSounds.Length)], 0.1f);
					}
				}
			}
		}


	}

	//checks input from the player
	void GetInput()
	{
		//only updates the lookdirection if we are recieving input
		if (Input.GetAxisRaw("Look Horizontal") != 0 || Input.GetAxisRaw("Look Vertical") != 0)
		{
			//the direction the player is looking
			lookDirection = new Vector3(Input.GetAxisRaw("Look Horizontal"), 0, Input.GetAxisRaw("Look Vertical"));
			//clamp the magnitude to prevent any unexpected results
			lookDirection.Normalize();

			//rotate the player to face the correct direction
			transform.rotation = Quaternion.LookRotation(lookDirection, Vector3.up);
		}

		//select the weapon the player wants to use
		if (Input.GetAxis("Weapon Horizontal") > 0)
		{
			//select the laser
			WeaponTypes = state.laser;
		}
		else if (Input.GetAxis("Weapon Horizontal") < 0)
		{
			//select the shotgun
			WeaponTypes = state.shotgun;
		}
		else if (Input.GetAxis("Weapon Vertical") > 0)
		{
			//select the grenades
			WeaponTypes = state.grenades;
		}
		else if (Input.GetAxis("Weapon Vertical") < 0)
		{
			//select the assault Rifle
			WeaponTypes = state.assaultRifle;
		}

		if (Input.GetAxis("Shoot") != 0)
		{
			Shoot();
		}
		else if (Input.GetAxis("Shoot") == 0)
		{
			laser.SetActive(false);
			playerLineLaser.enabled = false;
		}
	}

	//releases the player
	void StopDodging()
	{
		dodging = false;
	}

	//stops the dodge animation early
	void StopDodgeAnimation()
	{
		playerAnimator.SetFloat("dodgeXInput", 0);
		playerAnimator.SetFloat("dodgeZInput", 0);
		playerAnimator.StopPlayback();
	}

	//check if the laser is being used
	void LaserCheck ()
	{
		//if the player is not providing input for the weapons
		if (Input.GetAxisRaw("Shoot") == 0)
		{
			//ensure laser is off
			laser.SetActive(false);

            playerLineLaser.enabled = false;
        }
	}

	//this code runs when the player fires their weapon
	void Shoot()
	{
		//is the player using the the laser
		if(WeaponTypes == state.laser)
		{
			//turn on the laser gameobject
			laser.SetActive (true);
            laserBall.Play();
            laserBallEnd.Play();
            laserFlash.Play();                    

            playerLineLaser.enabled = true;
            playerLineLaser.SetPosition(0, gun.transform.position);
            playerLineLaser.SetPosition(1, endPoint.transform.position);
		}
		//is the player using the the shotgun
		else if(WeaponTypes == state.shotgun)
		{
			//ensure the laser is off
			laser.SetActive (false);
            playerLineLaser.enabled = false;

            //check if the cooldown has finished
            if (Time.time >= shotgunCooldown) 
			{
				//apply the cooldown time
				shotgunCooldown = Time.time + shotgunCooldownTime;

				//the number of degrees between the paths of each pellet
				float difference = degreesBetween / numberOfPellets;
				//how far left we need to create the first pellet
				float halfDifference = difference / 2;

				//for the number of pellets we have
				for (int i = 0; i < numberOfPellets; i++)
				{
					//creates a pellet and rotates it to its spot in the bullet spread range
					Instantiate (bulletShotgun, gun.transform.position, (gun.transform.rotation * Quaternion.Euler (0, (-halfDifference + i), 0)));
					playerAS.PlayOneShot(weaponSounds[1], 0.1f);
                    rifleFlash.Play();
                }
			}
		}
		//is the player using the the assault rifle
		else if(WeaponTypes == state.assaultRifle)
		{
			//ensure the laser is off
			laser.SetActive (false);
            playerLineLaser.enabled = false;

            //check if the cooldown has finished
            if (Time.time >= assaultRifleCooldown) 
			{
				//apply the cooldown time
				assaultRifleCooldown = Time.time + assaultRifleCooldownTime;

				//spawn the bullet
				Instantiate (bulletAssaultRifle, gun.transform.position, gun.transform.rotation);
				playerAS.PlayOneShot(weaponSounds[3], 0.4f);
				rifleFlash.Play();
			}
		}
		//is the player using the the grenades
		else if(WeaponTypes == state.grenades)
		{
			//ensure the laser is off
			laser.SetActive (false);
            playerLineLaser.enabled = false;

            //check if the cooldown has finished
            if (Time.time >= grenadeCooldown) 
			{
				playerAS.PlayOneShot(weaponSounds[2], 0.4f);

				//apply the cooldown time
				grenadeCooldown = Time.time + grenadeCooldownTime;

				Invoke("ThrowGrenade", 0.75f);
				playerAnimator.StopPlayback();
				playerAnimator.Play("grenade");
			}
		}
	}

	//spawns grenade
	void ThrowGrenade()
	{
		//spawn the grenade
		Instantiate(bulletGrenade, gun.transform.position, gun.transform.rotation);
	}

	//deals damage to the player
	public void DealDamage(int damageToDeal)
	{
		if (Time.time > painCooldown)
		{
			painCooldown += 5;
			playerAS.PlayOneShot(painSounds[Random.Range(0, painSounds.Length)], 0.2f);
		}
		
		//if damage is combined into instances
		if(damageInstances == true)
		{
			//if the cooldown for damage instances has finished
			if(Time.time > damageCooldown)
			{
				//apply the cooldown time
				damageCooldown = Time.time + damageCooldownTime;

				//if the player has shields
				if(shields > 0)
				{
					//deal damage to shields
					shields -= 1;
				}
				//if the player doesnt have shields
				else if(shields < 1)
				{
					//deal damage to health
					health -= damageToDeal;
				}
			}
		}
		//if damage is seperated into individual instances
		else if(damageInstances == false)
		{
			//if the player has shields
			if(shields > 0)
			{
				//deal damage to shields
				shields -= 1;
			}
			//if the player doesnt have any shields
			else if(shields < 1)
			{
				//deals damage to the player
				health -= damageToDeal;
			}
		}
	}

	//heals the player
	public void DealHealing(string stringToCheck, int amountToHeal)
	{
		//if the type is Shields
		if (stringToCheck == "Shields")
		{
			//add points to the shields stat
			shields += amountToHeal;

			playerAS.PlayOneShot(shieldUpSound);
		}
		//if the type is Health
		else if (stringToCheck == "Health")
		{
			//add points to the health stat
			health += amountToHeal;

			playerAS.PlayOneShot(healthUpSounds[Random.Range(0, healthUpSounds.Length)], .3f);
		}
	}
}