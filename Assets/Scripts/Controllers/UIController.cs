using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//Author/s:Bryce Prince
//Date:4/4/18
//Function:UI Controller
public class UIController : MonoBehaviour 
{
	public static UIController controller;

	[Header("UI References")]
	//these are used for displaying our ui
	//PC: Why are all these serialized?!?
	[SerializeField] private GameObject imageHealth;
	[SerializeField] private GameObject imageShields1;
	[SerializeField] private GameObject imageShields2;
	[SerializeField] private GameObject imageShields3;
	[SerializeField] private GameObject imageLaser;
	[SerializeField] private GameObject imageShotgun;
	[SerializeField] private GameObject imageAssaultRifle;
	[SerializeField] private GameObject imageGrenades;

	[SerializeField] private Slider healthBar;

	private PlayerControllerScript playerControllerScript;

	void Awake () {
		//setup singleton instance
		if (controller == null) {
			controller = this;
		} else {
			Destroy(this);
		}
		
	}
	// Use this for initialization
	void Start () 
	{
        //find all the references in the scene

        /*imageHealth 		= GameObject.Find("Image Health");
		imageShields1 		= GameObject.Find("Image Shields (1)");
		imageShields2 		= GameObject.Find("Image Shields (2)");
		imageShields3 		= GameObject.Find("Image Shields (3)");
		imageLaser 			= GameObject.Find("Image Laser");
		imageShotgun 		= GameObject.Find("Image Shotgun");
		imageAssaultRifle 	= GameObject.Find("Image Assault Rifle");
		imageGrenades 		= GameObject.Find("Image Grenades");
		*/
		//PC: Please assign these in the inspector, GO.Find is very inefficient. 
        imageHealth = GameObject.Find("Health");
        imageShields1 = GameObject.Find("ShieldImage1");
        imageShields2 = GameObject.Find("ShieldImage2");
        imageShields3 = GameObject.Find("ShieldImage3");
        imageLaser = GameObject.Find("PistolImage");
        imageShotgun = GameObject.Find("ShotgunImage");
        imageAssaultRifle = GameObject.Find("AssaultImage");
        imageGrenades = GameObject.Find("GrenadeImage");
        

        healthBar = imageHealth.GetComponent<Slider> ();

		playerControllerScript = GameObject.Find ("Player").GetComponentInParent<PlayerControllerScript> ();

		//turn off all the weapons
		UIWeapons (false, false, false, false);
	}

	//update runs once per frame
	void Update ()
	{
		//PC: Replacing with slider
		//create a float that we can scale our ui element to
		//float healthBarScale = ((float)playerControllerScript.health / 100);
		//scale the ui element to show our health
		//healthBar.localScale = new Vector3(healthBarScale, 1f, 1f);
		healthBar.value = playerControllerScript.health;
		//activate and deactivate shield ui according to current shield amount
		
		if(playerControllerScript.shields == 3)
		{
			imageShields1.SetActive (true);
			imageShields2.SetActive (true);
			imageShields3.SetActive (true);
		}
		else if(playerControllerScript.shields == 2)
		{
			imageShields1.SetActive (true);
			imageShields2.SetActive (true);
			imageShields3.SetActive (false);

		}
		else if(playerControllerScript.shields == 1)
		{
			imageShields1.SetActive (true);
			imageShields2.SetActive (false);
			imageShields3.SetActive (false);
		}
		else if(playerControllerScript.shields == 0)
		{
			imageShields1.SetActive (false);
			imageShields2.SetActive (false);
			imageShields3.SetActive (false);
		}
	}

	//turns the weapons ui on and off depending on which inputs in the ui
	public void UIWeapons(bool laser, bool shotgun, bool assaultRifle, bool grenade)
	{
		if(laser == true)
		{
			//turn on the laser ui texture
			imageLaser.SetActive (true);
		}
		else if(laser == false)
		{
			//turn off the laser ui texture
			imageLaser.SetActive (false);
		}

		if(shotgun == true)
		{
			//turn on the shotgun ui texture
			imageShotgun.SetActive (true);
		}
		else if(shotgun == false)
		{
			//turn off the shotgun ui texture
			imageShotgun.SetActive (false);
		}

		if(assaultRifle == true)
		{
			//turn on the assault Rifle ui texture
			imageAssaultRifle.SetActive (true);
		}
		else if(assaultRifle == false)
		{
			//turn off the assault Rifle ui texture
			imageAssaultRifle.SetActive (false);
		}

		if(grenade == true)
		{
			//turn on the grenade ui texture
			imageGrenades.SetActive (true);
		}
		else if(grenade == false)
		{
			//turn off the grenade ui texture
			imageGrenades.SetActive (false);
		}
	}

	//PC: Created an enum-based system. Turning all off, with one then back on is more efficient, and not noticable by the player. 
	public enum UIWeaponType {
		Laser,
		Shot,
		Assault,
		Grenade,
		None		
	};

	public void UIWeapons (UIWeaponType wType) {
		imageLaser.SetActive(false);
		imageShotgun.SetActive(false);
		imageAssaultRifle.SetActive(false);
		imageGrenades.SetActive(false);
		switch (wType){
			case UIWeaponType.Laser:
				imageLaser.SetActive(true);
				break;
			case UIWeaponType.Shot:
				imageShotgun.SetActive(true);
				break;
			case UIWeaponType.Assault:
				imageAssaultRifle.SetActive(true);
				break;
			case UIWeaponType.Grenade:
				imageGrenades.SetActive(true);
				break;
			default:
				break;
		}
	}
}