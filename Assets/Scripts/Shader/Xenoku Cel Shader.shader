﻿Shader "Xenoku/Cel"
{
	Properties 
	{
	_UnlitColor ("Unlit Color", Color) = (0.25,0.25,0.25,1)
	_DiffuseThreshold ("Lighting Threshold", Range(-1.1,1)) = 0.15
	_SpecColor ("Specular Material Color", Color) = (1,1,1,1) 
	_Shininess ("Shininess", Range(0.5,1)) = 1	
	_OutlineThickness ("Outline Thickness", Range(0,1)) = 0.0
	_MainTex ("Main Texture", 2D) = "" {}   
	}

	SubShader 
	{
		Pass 
		{
			Tags
			{ 
				"LightMode" = "ForwardBase" 
			}

			CGPROGRAM

			#pragma vertex vert 
			#pragma fragment frag

			#include "UnityCG.cginc"

			uniform float4 _LightColor0;
			uniform float4 _UnlitColor;
			uniform float _DiffuseThreshold;
			uniform float4 _SpecColor;
			uniform float _Shininess;
			uniform float _OutlineThickness;
			uniform sampler2D _MainTex;    

			uniform float4 _MainTex_ST;

			struct vertexInput 
			{
				float4 vertex : POSITION;
				float3 normal : NORMAL;
				float4 texcoord : TEXCOORD0;
			};

			struct vertexOutput 
			{
				float4 pos : SV_POSITION;
				float2 posWorld : TEXCOORD0;
				float3 normalDir : TEXCOORD1;
				float4 lightDir : TEXCOORD2;
				float3 viewDir : TEXCOORD3;
				float2 uv : TEXCOORD5;
			};

			vertexOutput vert(vertexInput input)
			{
				vertexOutput output;

				output.normalDir = normalize(mul(float4(input.normal, 0.0), unity_WorldToObject).xyz);

				float4 posWorld = mul(unity_ObjectToWorld, input.vertex);

				output.viewDir = normalize(_WorldSpaceCameraPos.xyz - posWorld.xyz);

				float3 fragmentToLightSource = (_WorldSpaceCameraPos.xyz - posWorld.xyz);

				output.lightDir = float4(normalize(lerp(_WorldSpaceLightPos0.xyz, fragmentToLightSource, _WorldSpaceLightPos0.w)), lerp(1.0, 1.0 / length(fragmentToLightSource), _WorldSpaceLightPos0.w));

				output.pos = UnityObjectToClipPos(input.vertex);

				output.posWorld = input.texcoord;

				output.uv = TRANSFORM_TEX(input.texcoord, _MainTex);

				return output;
			}

			float4 frag(vertexOutput input) : COLOR
			{
				float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);

				float nDotL = saturate(dot(input.normalDir, lightDirection.xyz));

				float diffuseCutoff = saturate((max(_DiffuseThreshold, nDotL) - _DiffuseThreshold) * 1000);

				float specularCutoff = saturate(max(_Shininess, dot(reflect(-lightDirection.xyz, input.normalDir), input.viewDir)) - _Shininess) * 1000;

				float outlineStrength = saturate((dot(input.normalDir, input.viewDir) - _OutlineThickness) * 1000);

				float3 ambientLight = (1 - diffuseCutoff) * _LightColor0.xyz;
				float3 diffuseReflection = (1 - specularCutoff) * _LightColor0.xyz * diffuseCutoff;
				float3 specularReflection = _SpecColor.xyz * specularCutoff;

				float3 combinedLight = (ambientLight + diffuseReflection) * outlineStrength + specularReflection;


				return float4(combinedLight, 1.0) * tex2D(_MainTex, input.uv);
			}

			ENDCG
		}

		Pass
		{
			Tags
			{
				"LightMode" = "ForwardAdd"
			}

			Blend one one

			CGPROGRAM

			#pragma vertex vert 
			#pragma fragment frag

			#include "UnityCG.cginc"

			uniform float4 _LightColor0;
			uniform float4 _UnlitColor;
			uniform float _DiffuseThreshold;
			uniform float4 _SpecColor;
			uniform float _Shininess;
			uniform float _OutlineThickness;
			uniform sampler2D _MainTex;

			uniform float4 _MainTex_ST;

			struct vertexInput
			{
				float4 vertex : POSITION;
				float3 normal : NORMAL;
				float4 texcoord : TEXCOORD0;
			};

			struct vertexOutput
			{
				float4 pos : SV_POSITION;
				float3 texcoord : TEXCOORD0;
				float3 posWorld : TEXCOORD1;
				float3 normalDir : TEXCOORD2;
				float4 lightDir : TEXCOORD3;
				float3 viewDir : TEXCOORD4;
				float2 uv : TEXCOORD5;
			};

			vertexOutput vert(vertexInput input)
			{
				vertexOutput output;

				output.normalDir = normalize(mul(float4(input.normal, 0.0), unity_WorldToObject).xyz);

				float4 posWorld = mul(unity_ObjectToWorld, input.vertex);

				output.viewDir = normalize(_WorldSpaceCameraPos.xyz - posWorld.xyz); 

				float3 fragmentToLightSource = (_WorldSpaceCameraPos.xyz - posWorld.xyz);

				output.lightDir = float4(normalize(lerp(_WorldSpaceLightPos0.xyz , fragmentToLightSource, _WorldSpaceLightPos0.w)), lerp(1.0 , 1.0 / length(fragmentToLightSource), _WorldSpaceLightPos0.w));

				output.pos = UnityObjectToClipPos(input.vertex);

				output.posWorld = mul(unity_ObjectToWorld, input.vertex);

				output.uv = TRANSFORM_TEX(input.texcoord, _MainTex);

				return output;
			}

			float4 frag(vertexOutput input) : COLOR
			{
				float3 lightDirection = float3(0, 0, 0);
				float3 combinedLight = float3(0, 0, 0);

				if (0.0 != _WorldSpaceLightPos0.w)
				{
					float3 vertexToLightSource = _WorldSpaceLightPos0.xyz - input.posWorld;

					lightDirection = normalize(vertexToLightSource);
				}

				float nDotL = saturate(dot(input.normalDir, lightDirection.xyz));

				float diffuseCutoff = saturate((max(_DiffuseThreshold, nDotL) - _DiffuseThreshold) * 1000);

				float specularCutoff = saturate(max(_Shininess, dot(reflect(-lightDirection.xyz, input.normalDir), input.viewDir)) - _Shininess) * 1000;
				
				float3 ambientLight = (1 - diffuseCutoff) * _UnlitColor.xyz;
				float3 diffuseReflection = (1 - specularCutoff) * _LightColor0.xyz * diffuseCutoff;
				float3 specularReflection = _SpecColor.xyz * specularCutoff;

				float outlineStrength = saturate((dot(input.normalDir, input.viewDir) - _OutlineThickness) * 1000);
				
				//unlit areas
				if (dot(input.normalDir, lightDirection) < 0.0)
				{
					combinedLight = _UnlitColor * tex2D(_MainTex, input.texcoord);
				}

				//lit areas
				else if (dot(input.normalDir, lightDirection) > 0.0)
				{
					combinedLight = ((ambientLight + diffuseReflection) * outlineStrength + specularReflection) * tex2D(_MainTex, input.texcoord);
				}

				return float4(combinedLight, 1.0);
			}

		ENDCG
		}
	} 

	Fallback "Specular"
}