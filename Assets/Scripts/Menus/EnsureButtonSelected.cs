﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class EnsureButtonSelected : MonoBehaviour {
    [SerializeField]
    Button buttonDefault;

    void Update () {

        if (Input.GetAxis("Vertical") != 0f) {
            if (!IsGameObjectSelected()) {
                buttonDefault.Select();
            }
        }
		
	}

    bool IsGameObjectSelected() {
        if (EventSystem.current.currentSelectedGameObject != null)
            return true;

        return false;
    }
}
