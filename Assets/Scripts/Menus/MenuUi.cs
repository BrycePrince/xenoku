﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuUi : MonoBehaviour {

    [SerializeField]
    private GameObject Pointer;

    [SerializeField]
    private AudioClip[] ButtonSound;

    [SerializeField]
    private AudioSource buttonPlayer;


	// Use this for initialization
	void Start ()
    {
        buttonPlayer = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    public void PointerEnable()
    {
        Pointer.SetActive(true);
        PlayButtonSOund();
    }

    public void PointerDisable()
    {
        Pointer.SetActive(false);
    }

    public void PlayButtonSOund()
    {
        buttonPlayer.clip = ButtonSound[Random.Range(0, 2)];
        buttonPlayer.Play();
    }
}
