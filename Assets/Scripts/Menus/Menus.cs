﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

public class Menus : MonoBehaviour {

    [SerializeField]
    private AudioClip MenuStart;

    [SerializeField]
    private AudioSource menuSource;

    public GameObject mainButton;

	// Use this for initialization
	void Start ()
    {
        menuSource = GetComponent<AudioSource>();
        menuSource.clip = MenuStart;

        EventSystem.current.SetSelectedGameObject(mainButton);
	}


    // Update is called once per frame
    void Update () {
		
	}

    //Load the first level
    public void StartGame()
    {
        SceneManager.LoadScene("Level 1 Final");
        Debug.Log("Start Game");
    }

    //Exit the game
    public void ExitGame()
    {
        Application.Quit();
        Debug.Log("Close Game");
    }

    //Load on winning game
    public void WinScene()
    {
        SceneManager.LoadScene("WinScene");
        Debug.Log("Player Wins");
    }

    //Load on losing game
    public void DeathScene()
    {
        SceneManager.LoadScene("LoseScene");
        Debug.Log("Player Lose");
    }

    public void PauseGame()
    {
        Time.timeScale = 0;
        Debug.Log("Game Paused");
    }

    public void ContinueGame()
    {
        Time.timeScale = 1;
        Debug.Log("Game Continued");
    }

    public void MainMenu()
    {
        SceneManager.LoadScene("MenuScene");
        Debug.Log("Go to main menu");
    }

    public void CheckpointContinue()
    {

    }

    public void CreditsLoad()
    {
        SceneManager.LoadScene("Credits");
    }
}
